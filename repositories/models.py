
class Repository:
    def __init__(self, id, name, url, ssh_url, last_updated, source):
        self.id = id
        self.name = name
        self.ssh_url = ssh_url
        self.url = url
        self.last_updated = last_updated
        self.source = source

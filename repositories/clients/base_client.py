import requests

from abc import ABC, abstractmethod
from repositories.exceptions import RMSException


class RMSClient(ABC):
    """
    This Abstract base class serves as an "Interface" for all
    RMS clients. Implement a client by configuring its settings and
    implementing the search method. A serializer should be added to
    validate and convert the response.
    """

    def __init__(self, name, base_url):
        self.name = name
        self.base_url = base_url
        super().__init__()

    @abstractmethod
    def search(self, query, limit):
        """
        Query the RMS using a search term.
        :param query: str search term
        :param query: int limit amount of results
        :return: dict repositories found.
        """

    def _send_request(
            self, endpoint,
            method='GET',
            data=None,
            headers=None):

        response = requests.request(
            method,
            endpoint,
            params=data,
            headers=headers,
            verify=False
        )

        if not response.ok:
            raise RMSException(
                f"Something went wrong with the request to RMS {self.name}."
                f"Response: {vars(response)}"
            )

        return response

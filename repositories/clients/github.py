from .base_client import RMSClient


class GitHubClient(RMSClient):
    def __init__(self,
                 name='github',
                 base_url='https://api.github.com/search/repositories'):
        self.name = name
        self.base_url = base_url

    def search(self, query, limit):
        headers = None

        params = {
            'q': query,
            'page': 1,
            'per_page': limit
        }

        response = self._send_request(
            endpoint=self.base_url,
            data=params,
            headers=headers
        ).json()

        return response['items']

from .base_client import RMSClient


class GitLabClient(RMSClient):
    def __init__(self, name='gitlab',
                 base_url='https://gitlab.com/api/v4/projects'):
        self.name = name
        self.base_url = base_url

    def search(self, query, limit):
        headers = None

        params = {
            'page': 1,
            'per_page': limit
        }

        if query is not None:
            params['search'] = query

        response = self._send_request(
            endpoint=self.base_url,
            data=params,
            headers=headers
        ).json()

        return response

from rest_framework import serializers

from .models import Repository


class RepositorySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=False)
    name = serializers.CharField(max_length=256)
    url = serializers.CharField(max_length=256)
    ssh_url = serializers.CharField(max_length=256)
    last_updated = serializers.CharField(max_length=256)
    source = serializers.CharField(max_length=256)


class GitHubSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=False)
    name = serializers.CharField(max_length=256)
    url = serializers.CharField(max_length=256)
    ssh_url = serializers.CharField(max_length=256)
    updated_at = serializers.CharField(max_length=256)

    def create(self, validated_data):
        return Repository(
            id=validated_data['id'],
            name=validated_data['name'],
            url=validated_data['url'],
            ssh_url=validated_data['ssh_url'],
            last_updated=validated_data['updated_at'],
            source='github'
        )


class GitLabSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=False)
    name = serializers.CharField(max_length=256)
    web_url = serializers.CharField(max_length=256)
    ssh_url_to_repo = serializers.CharField(max_length=256)
    last_activity_at = serializers.CharField(max_length=256)

    def create(self, validated_data):
        return Repository(
            id=validated_data['id'],
            name=validated_data['name'],
            url=validated_data['web_url'],
            ssh_url=validated_data['ssh_url_to_repo'],
            last_updated=validated_data['last_activity_at'],
            source='gitlab'
        )

from rest_framework.response import Response
from rest_framework import views, status

from .clients.github import GitHubClient
from .clients.gitlab import GitLabClient
from .serializers import (GitHubSerializer,
                          GitLabSerializer,
                          RepositorySerializer)
from .services import fetch_repositories


class RepositoryView(views.APIView):
    """
    Base set of views for Repositories.

    * Requires search param and configured clients.
    """

    # Map clients to serializers
    CLIENTS = [
        (GitHubClient(), GitHubSerializer(many=True)),
        (GitLabClient(), GitLabSerializer(many=True)),
    ]

    def get(self, request):
        """
        Return a list of repositories aggregated from configured clients.
        :Param search str Term to search for
        """
        search_query = self.request.query_params.get('search', None)
        if search_query is None:
            return Response(data="Missing search param",
                            status=status.HTTP_400_BAD_REQUEST)

        repositories = fetch_repositories(query=search_query,
                                          limit=5,
                                          clients=self.CLIENTS)
        serializer = RepositorySerializer(instance=repositories, many=True)

        response = {
            "total_repositories": len(serializer.data),
            "clients_queried": len(self.CLIENTS),
            "repositories": serializer.data
        }
        return Response(data=response, status=status.HTTP_200_OK)

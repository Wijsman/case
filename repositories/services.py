import logging

from .exceptions import RMSException


def fetch_repositories(query, clients, limit=5):
    """
    Service responsible for fetching repositories from multiple providers.
    As the application complexifies, logic for building search queries
    and results is done here.

    :Param query str of search query.
    :Param clients list containing tuples of Client and Serializers
    :Param limit int of repositories per client to return
    :Return list of all repositories.
    """
    repositories = []

    for client in clients:
        try:
            response = client[0].search(query, limit)
            client[1].initial_data = response

            if client[1].is_valid():
                repository_objects = client[1].create(client[1].validated_data)
                repositories.extend(repository_objects)
            else:
                raise RMSException(
                    f"Response from RMS {client[1].name} not formatted as "
                    f"expected. Response: {vars(response)}"
                )

        # Only log, to allow failing of one client without blocking.
        except Exception as e:
            logging.error(e)

    return repositories

from django.urls import path
from rest_framework import routers
from django.views.generic.base import RedirectView
from . import views

# Register viewsets
router = routers.DefaultRouter()

# Static routes
API_PREFIX = 'api/v1'
urlpatterns = [
    path('', RedirectView.as_view(url=f'{API_PREFIX}/repositories')),
    path(f'{API_PREFIX}/repositories',
         views.RepositoryView.as_view(), name="repositories"),
]

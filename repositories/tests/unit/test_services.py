from unittest import TestCase
from unittest.mock import MagicMock, patch

from repositories.services import fetch_repositories
from repositories.models import Repository


# Unit tests
class RepositoryServiceTests(TestCase):
    def setUp(self):
        # Shared mocking variables
        self.query = "search_term"
        self.limit = 11

        self.search_response = [
            {
                "id": 4164482,
                "name": "django",
                "url": "https://api.github.com/repos/django/django",
                "ssh_url": "git@github.com:django/django.git",
                "updated_at": "2020-12-05T09:37:26Z",
            }
        ]

        self.repository = Repository(
            id=self.search_response[0]['id'],
            name=self.search_response[0]['name'],
            url=self.search_response[0]['url'],
            ssh_url=self.search_response[0]['url'],
            last_updated=self.search_response[0]['updated_at'],
            source='github'
        )
        self.created_repositories = [self.repository]

        self.client = MagicMock()
        self.client.search = MagicMock(return_value=self.search_response)

        self.serializer = MagicMock()
        self.serializer.create = MagicMock(
            return_value=self.created_repositories)
        self.serializer.validated_data = MagicMock(
            return_value=self.search_response[0])

        self.mock_clients = [(self.client, self.serializer)]

    def test_fetch_repositories(self):
        """
        Assert happy flow in repository fetching service is positive.
        """
        # Arrange
        self.serializer.is_valid = MagicMock(return_value=True)

        # Act
        result = fetch_repositories(self.query, self.mock_clients, self.limit)

        # Assert
        self.client.search.assert_called_with(self.query, self.limit)
        self.serializer.is_valid.assert_called()
        self.serializer.create.assert_called_with(
            self.serializer.validated_data)
        self.assertEqual(result, self.created_repositories)

    def test_fetch_repositories_invalid_reponse(self):
        """
        Assert unexpected responses from a provider to be handled.
        """
        # Arrange
        self.serializer.is_valid = MagicMock(return_value=False)

        # Act
        result = fetch_repositories(self.query, self.mock_clients, self.limit)

        # Assert
        self.client.search.assert_called_with(self.query, self.limit)
        self.serializer.is_valid.assert_called()
        self.assertEqual(result, [])

    @patch('logging.error')
    def test_fetch_repositories_failed_request(self, mock_logger):
        """
        Assert error handling in repository fetching service are logged.
        """
        # Arrange
        self.client.search = MagicMock(side_effect=Exception())

        # Act
        fetch_repositories(self.query, self.mock_clients, self.limit)

        # Assert
        self.client.search.assert_called_with(self.query, self.limit)
        mock_logger.assert_called()
        self.serializer.is_valid.assert_not_called()

from unittest import TestCase
from unittest.mock import MagicMock

from repositories.clients.github import GitHubClient
from repositories.clients.gitlab import GitLabClient


# Test RMS clients
class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


class RMSClientTests(TestCase):
    def test_github_search(self):
        """
        Assert github client happy flow works as expected.
        """
        # Arrange
        query = 'django'
        limit = 10

        response_data = {
            "total_count": 420,
            "incomplete_results": False,
            "items": [
                {
                    "id": 4164482,
                    "name": "django",
                    "url": "https://api.github.com/repos/django/django",
                    "ssh_url": "git@github.com:django/django.git",
                    "updated_at": "2020-12-05T09:37:26Z",
                }
            ]
        }
        search_response = MockResponse(response_data, 200)

        client = GitHubClient(
            name='github',
            base_url='https://api.github.com/search/repositories'
        )
        client._send_request = MagicMock(return_value=search_response)

        # Act
        result = client.search(query, limit)

        # Assert
        client._send_request.assert_called_with(
            data={'page': 1, 'per_page': 10, 'q': 'django'},
            endpoint='https://api.github.com/search/repositories',
            headers=None
        )
        self.assertEqual(result, response_data['items'])

    def test_gitlab_search(self):
        """
        Assert gitlab client happy flow works as expected.
        """
        # Arrange
        query = 'django'
        limit = 10

        response_data = [
            {
                "id": 22920147,
                "name": "vue-django",
                "web_url": "https://gitlab.com/jhampton/vue-django",
                "ssh_url": "git@gitlab.com:django/django.git",
                "last_activity_at": "2020-12-05T09:37:26Z"
            }
        ]

        search_response = MockResponse(response_data, 200)

        client = GitLabClient(
            name='gitlab',
            base_url='https://gitlab.com/api/v4/projects'
        )
        client._send_request = MagicMock(return_value=search_response)

        # Act
        result = client.search(query, limit)

        # Assert
        client._send_request.assert_called_with(
            data={'page': 1, 'per_page': 10, 'search': 'django'},
            endpoint='https://gitlab.com/api/v4/projects',
            headers=None
        )
        self.assertEqual(result, response_data)

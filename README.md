### OfficeApp Case

This simple Django application serves a REST-API with a single endpoint.

Its purpose is to aggregate repositories from multiple Repository Management Systems (RMS) such as
Github, Gitlab, Bitbucket etc.

### Notes / Experience while developing

Fun little case!

Total time spent: +- 4 hours on the logic using a stopwatch, at 4 hour mark wrapped up and wrote comments etc.
Eventhough this could have been a single file application, decided to do this in Django since this is your frameworks as well.
Was a bit thrown off by working with django without a database / SQLalchemy but i tried to keep the "Django" project structure
and still use models/serializers which was slightly weird, but a fun experiment.

Was a bit torn between: "Overengineering" and "Too simple", but tried to take a middle ground.

In the application:
- 1 endpoint (GET /api/v1/repositories)
- "client" based approach with a baseclass / interface for the RMS providers. 
- Used DRF Serializers to read/validate the client reponse, with an additional Serializer to convert to "Our" standard model.
- Basic unit tests for the fetching / aggregation logic.

Things that could/should be added with more time:
- Proper error / exception handling
- Not too happy about the Client/Serializer mapping I used, could add dependency injection.
- Pagination
- Filtering params
- Add bitbucket client
- Functional/integration test
- Caching reponses

## Set up

Run the following commands to build the project.

### Docker

```
cp example.env .env
docker build -t officeapp:case .
docker run -p 8000:8000 --env-file=.env officeapp:case
curl --location --request GET 'http://localhost:8085/api/v1/repositories?search=django'
```

### Virtual environment

```
python -m venv env
source env/bin/activate
pip install -r requirements.txt
python manage.py runserver
curl --location --request GET 'http://localhost:8085/api/v1/repositories?search=django'
```

The application will now be up and running.

### Running tests

```
manage.py test
```

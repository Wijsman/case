FROM python:3.8

WORKDIR /app

# Shell/Ops Tools
RUN apt-get update && \
    apt-get install -y bash vim
RUN pip install --no-cache-dir flake8 "uWSGI<2.1"


# Install Python dependencies
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

# Copy our codebase into the container
COPY . .

# Collectstatic, fake these values because our codebase makes them required.
RUN ./manage.py collectstatic --noinput

# Deploy Parameters
ENV WORKERS=2
ENV HOST=0.0.0.0
ENV PORT=8000

EXPOSE ${PORT}

CMD uwsgi --http ${HOST}:${PORT} --processes ${WORKERS} --static-map /static=/static --module repo_browser.wsgi:application
